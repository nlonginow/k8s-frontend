package com.baeldung.spring.cloud.kubernetes.frontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.UnknownHostException;

@RestController
@SpringBootApplication
public class KubernetesFrontendApplication {

	public static void main(String[] args) {
		SpringApplication.run(KubernetesFrontendApplication.class, args);
	}

	@GetMapping
	public String helloWorld() throws UnknownHostException {

		System.out.println("You have reached the demo-frontend pod");
		RestTemplate restTemplate = new RestTemplate();
		String resourceUrl = "http://demo-backend:8080";
		ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
		String firstBody = response.getBody();
		resourceUrl = resourceUrl + "/first";
		response = restTemplate.getForEntity(resourceUrl, String.class);
		String wholeMsg = firstBody + " " + response.getBody();
		return "Message from backend is: " + wholeMsg;
	}
}
