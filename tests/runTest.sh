#!/bin/sh
echo 'Starting test... [Timeout per Curl: 5min, Curl Retries: 15]'
n=1
until [ $n -ge 16 ]
do
 result=`curl --retry-delay 15 --connect-timeout 300 --retry 5  -sL -w "%{http_code}\\n" "http://mywork-local.teradata.com:8080/ws/v2/assoctracking/types" -X GET -H "Authorization:Basic bmwxODYwMDY6UGFzczZ3b3Jk" -o /dev/null | grep 200`
 if [ "$result" == "200" ]; then
        echo 'Success'
        exit 0
 else
        echo 'Failed attempt ' $n
 fi

n=$[$n+1]
sleep 5
echo 'Retrying...'
done

